data "aws_vpc" "vpc_id" {
  filter {
    name   = "tag:Name"
    values = ["challenge week VPC"]
  }
}

data "aws_subnet" "subnit_id" {
  filter {
    name   = "tag:Name"
    values = ["Kibana subnet"]
  }
}

#EC2 Instance 
resource "aws_instance" "Kibana-server" {
  ami           = "ami-08ca3fed11864d6bb" #ubuntu 20.04 lts 
  instance_type = "t2.medium"
  subnet_id = data.aws_subnet.subnit_id.id
  vpc_security_group_ids = [aws_security_group.Kibana-security-group.id]
  key_name = "ELK-Application-SSH-Keys"
  monitoring = true
  associate_public_ip_address = true
  user_data =<<EOF
  #! /bin/bash
  sudo apt update
  sudo apt install openssh-server
  sudo systemctl status ssh
  sudo ufw allow ssh
EOF
  tags = {
    Name = "Kibana"
  }
}

#Security Group 
resource "aws_security_group" "Kibana-security-group" {
  name        = "Kibana-sg"
  description = "Allow Kibana traffic"
  vpc_id      = data.aws_vpc.vpc_id.id

  ingress {
    description      = "Kibana" #5601 TCP, Kibana (web interface)
    from_port        = 5601
    to_port          = 5601
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

   ingress {
    description      = "Requests"
    from_port        = 9200
    to_port          = 9200
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

   ingress {
    description      = "Communication"
    from_port        = 9300
    to_port          = 9300
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

   ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"] #lock this down in future for access only for team (ips)
  }

   ingress {
    description      = "HTTP"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

   ingress {
    description      = "HTTPS"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "Kibana-security-group"
  }
}

#EIP 
resource "aws_eip" "kibana_eip" {
  instance = aws_instance.Kibana-server.id
  vpc      = true
}

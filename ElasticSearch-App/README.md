# README #

This repository is part of Team 2's ELK Stack Challenge Project.

### What is this repository for? ###

This repository contains the applications that are part of the ELK stack.

* branch - feature-elasticsearch-ansible *
This branch contains the working Ansible playbook that installs the ElasticSearch app on a target server.

Files created during this branch:
- ansible-workspace/inventory.hosts:  this file will contain the ip of the server that will host the app
- ansible-workspace/ansible.cfg: this file hosts the default configuration for Ansible
- ansible-workspace/deploy_elasticsearch.yml - this file is the playbook mentioned above.

## Notes ##

1. In the playbook, ElasticSearch's network host can be configured, currently it uses network.hosts: localhost
2. The port binding is set to http.port: 9200.

data "aws_vpc" "vpc_id" {
  filter {
    name   = "tag:Name"
    values = ["challenge week VPC"]
  }
}

data "aws_subnet" "subnit_id" {
  filter {
    name   = "tag:Name"
    values = ["Monitoring subnet"]
  }
}

#EC2 Instance 
resource "aws_instance" "Elasticsearch-server" {
  ami           = "ami-08ca3fed11864d6bb" #ubuntu 20.04 lts 
  instance_type = "t2.medium"
  subnet_id = data.aws_subnet.subnit_id.id
  vpc_security_group_ids = [aws_security_group.Elasticsearch-security-group.id]
  key_name = "ELK-Application-SSH-Keys"
  monitoring = true
  tags = {
    Name = "Elasticsearch"
  }
}

#Security Group 
resource "aws_security_group" "Elasticsearch-security-group" {
  name        = "Elasticsearch-sg"
  description = "Allow Elasticsearch traffic"
  vpc_id      = data.aws_vpc.vpc_id.id

  ingress {
    description      = "Kibana" #5601 TCP, Kibana (web interface)
    from_port        = 5601
    to_port          = 5601
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

   ingress {
    description      = "Logstash"
    from_port        = 5044
    to_port          = 5044
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

   ingress {
    description      = "Requests"
    from_port        = 9200
    to_port          = 9200
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

   ingress {
    description      = "Communication"
    from_port        = 9300
    to_port          = 9300
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

   ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"] #lock this down in future for access only for team (ips)
  }

  tags = {
    Name = "Elasticsearch-security-group"
  }
}

#EIP
resource "aws_eip" "Elasticsearch_eip" {
  instance = aws_instance.Elasticsearch-server.id
  vpc      = true
}
#Data Source for VPC
data "aws_vpc" "vpc_id" {
  filter {
    name   = "tag:Name"
    values = ["challenge week VPC"]
  }
}

data "aws_subnet" "monitoring" {
  filter {
    name   = "tag:Name"
    values = ["Monitoring subnet"]
  }
}

#EC2 Instance 
resource "aws_instance" "Logstash_server" {
  ami           = "ami-08ca3fed11864d6bb" #ubuntu 20.04 lts 
  instance_type = "t2.medium"
  subnet_id = data.aws_subnet.monitoring.id
  vpc_security_group_ids = [aws_security_group.Logstash-security-group.id]
  key_name = "ELK-Application-SSH-Keys"
  monitoring = true
    tags = {
    Name = "Logstash"
  }
}

#Security Group 
resource "aws_security_group" "Logstash-security-group" {
  name        = "Logstash-sg"
  description = "Allow Logstash traffic"
  vpc_id      = data.aws_vpc.vpc_id.id

  ingress {
    description      = "Logstash" #By default, Logstash will use port 9600. If this port is in use when the server starts
    from_port        = 9600
    to_port          = 9600
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

   ingress {
    description      = "Requests"
    from_port        = 9200
    to_port          = 9200
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

   ingress {
    description      = "Communication"
    from_port        = 9300
    to_port          = 9300
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

   ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"] #lock this down in future for access only for team (ips)
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "Logstash-security-group"
  }
}



#EIP
resource "aws_eip" "Logstash_eip" {
  instance = aws_instance.Logstash_server.id 
  vpc      = true
}

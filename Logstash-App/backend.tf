terraform {
    backend "s3" {
    bucket = "elk-stack-team2"
    encrypt = true
    key = "terraform/logstash/terraform.tfstates"
    region = "eu-west-1"
    profile = "challenge-week"
  }
}